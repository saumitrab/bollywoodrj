Bollywood RJ
------------

This is an Alexa skill that looks for community radio available for Bollywood songs.
It makes easy to play radio stations with comfort of simple interactions with Alexa.

This skills takes away the pain of remembering the Radio Station names or frequency.

This has been build with the help of:

http://www.radio-browser.info
Approval to use the service: http://www.radio-browser.info/gui/#/

To invoke the skill, just say 'Alexa, [play|start] Bollywood RJ'

To move to a different station, say 'Alexa, next'

To stop the skill, say 'Alexa, Stop'.
