'use strict';

const http = require('http');
const https = require('https');
const URL = require('url').URL;

const { promisify } = require('util');

https.get[promisify.custom] = function getAsync(options) {
  return new Promise((resolve, reject) => {
    https.get(options, (response) => {
      response.end = new Promise((resolve) => response.on('end', resolve));
      resolve(response);
    }).on('error', reject);
  });
};

http.get[promisify.custom] = function getAsync(options) {
  return new Promise((resolve, reject) => {
    http.get(options, (response) => {
      response.end = new Promise((resolve) => response.on('end', resolve));
      resolve(response);
    }).on('error', reject);
  });
};

const asyncHttpGet = promisify(http.get);
const asyncHttpsGet = promisify(https.get);

function replaceHttpWithHttps(urlString) {
  let url = new URL(urlString);
  url.protocol = 'https';
  return url.href;
}

module.exports = { asyncHttpGet, asyncHttpsGet, replaceHttpWithHttps };
