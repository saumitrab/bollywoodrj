'use strict';

const assert = require('assert');
const URL = require('url').URL;

const fs = require('fs');
const path = require('path');

const { asyncHttpGet, asyncHttpsGet, replaceHttpWithHttps } = require('./http-helpers');
const { USER_AGENT } = require('./defaults');

function run() {

  const options = {
    hostname: 'www.radio-browser.info',
    path: '/webservice/json/stations/bylanguage/hindi',
    method: 'GET',
    headers: {
      'user-agent': USER_AGENT,
    }
  }
  
  asyncHttpGet(options)
    .then(response => {
      let data = '';
      response.on('data', chunk => data+= chunk);
      response.on('end', () => processData(JSON.parse(data)));
    })
    .catch(error => console.log(`Error: ${error.message}`));
}

function hasMantra(station) {
  return !!station.tags.match(/spiritual|devoational|mantra|krishna|rama/i);
}

function hasHttps(urlString) {
  const httpsUrlString = replaceHttpWithHttps(urlString);

  return asyncHttpsGet(httpsUrlString)
    .then(() => true)
    .catch(() => false);
}

function isInBlacklist(station) {
  if (station.name.match(/NDTV India|ABP News TV|India TV/i)) {
    return true;
  }
  if (station.tags.match(/Gujarati/i)) {
    return true;
  }
  return false;
}

async function processData(jsonData) {
  console.log(`Processing ${jsonData.length} entries...`);

  let filteredStations = [];
  for (let i = 0; i < jsonData.length; i++) {
    let station = jsonData[i];
    if (hasMantra(station)) {
      console.log(`Skipping station ${station.name} for mantra tags ${station.tags}`);
      continue;
    }
    if (isInBlacklist(station)) {
      console.log(`Skipping station ${station.name} due to blacklist`);
      continue;
    }
    try {
      const httpsSupport = await hasHttps(station.url);
      if (!httpsSupport) {
        console.log(`Skipping station ${station.name} for http only url ${station.url}`);
        continue;
      } else {
        console.log(`Updating station.url to https ${station.name}`);
        station.url = replaceHttpWithHttps(station.url);
      }
    } catch(err) {
      console.log(`Skipping station ${station.name} due to error while checking https ${err.message}`);
      continue;
    }
    
    filteredStations.push(station);
  };

  console.log(`Found ${filteredStations.length} stations!`);
  fs.writeFileSync(path.join(__dirname, 'stations.json'), JSON.stringify(filteredStations, null, 2));
}

if (process.env.TEST) {
  tests();
} else {
  run();
}

async function tests() {
  let station = {};
  station.tags = 'Hare Rama, spiritual';
  assert.equal(hasMantra(station), true);
  station.tags = 'hindi';
  assert.equal(hasMantra(station), false);

  assert.equal(replaceHttpWithHttps('http://example.com/?what=1'), 'https://example.com/?what=1');

  let urlString = 'http://indiatvnews-lh.akamaihd.net/i/ITV_1@199237/index_1_av-p.m3u8';
  assert.equal(await hasHttps(urlString), true);

  urlString = 'http://198.101.15.90:8140/;';
  assert.equal(await hasHttps(urlString), false);

  const rawData = require('./raw-data');
  try {
    await processData(rawData);
  } catch(err) {
    console.log(`Error while testing processData ${err.message}`);
  }
}
