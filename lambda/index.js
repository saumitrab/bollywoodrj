
'use strict';

const radioStations = require('./stations');
const { asyncHttpGet } = require('./http-helpers');
const { USER_AGENT } = require('./defaults')

exports.handler = async (event, context) => {
  const request = event.request;
  switch (request.type) {
    case 'LaunchRequest':
      handleLaunchRequest(context);
      break;

    case 'IntentRequest':
      handleIntentRequest(context, request);
      break;

    case 'SessionEndedRequest':
      handleStopMusicRequest(context);
      break;

    default:
      context.fail(`Error: Unknown intent type: ${request.type}`);
      break;
  }
};

function handleLaunchRequest(context) {
  handlePlayIntent(context)
}

function handleIntentRequest(context, request) {
  switch (request.intent.name) {
    case 'AMAZON.PauseIntent':
    case 'AMAZON.StopIntent':
      handleStopMusicRequest(context);
      break;
    case 'AMAZON.NextIntent':
      handlePlayIntent(context);
      break;
    case 'PlayIntent':
      handlePlayIntent(context);
      break;
    case 'AudioPlayer.PlaybackStarted':
    case 'AudioPlayer.PlaybackStopped':
      // no-op
      break;
    default:
      context.fail(`Error: Unknown Intent ${request.intent.name}`);
      break;
  }
}

function omit(obj, key) {
  const { [key]: deletedKey, ...otherKeys } = obj;
  return otherKeys;
}

function pickRadio() {
  const randomIndex = Math.floor(Math.random() * radioStations.length);
  return radioStations[randomIndex];
}

function handlePlayIntent(context) {
  const options = {};
  try {
    const station = pickRadio();
    const audioDirective = buildAudioDirective(station);
    if (audioDirective) {
      options.speechText = `Playing ${station.name}`;
      options.directives = [audioDirective];
    } else {
      options.speechText = `There was a problem.`;
    }
    options.shouldEndSession = true;
    context.succeed(createResponse(options));
  } catch (e) {
    context.fail("Exception " + e);
  }
}

function handleStopMusicRequest(context) {
  let options = {};
  options.shouldEndSession = true;
  options.speechText = "Thanks for listening with Bollywood RJ";
  options.directives = [{ "type": "AudioPlayer.Stop" }];
  try {
    context.succeed(createResponse(options));
  } catch (e) {
    context.fail("Exception " + e);
  }
}


function createResponse(options) {
  const resp = {
    version: "1.0",
    response: {
      outputSpeech: {
        type: "PlainText",
        text: options.speechText,
      },
      shouldEndSession: options.shouldEndSession,
    }
  };

  if (options.repromptText) {
    resp.response.reprompt = {
      outputSpeech: {
        type: "PlainText",
        text: options.repromptText,
      }
    }
  }

  if (options.directives) {
    resp.response.directives = options.directives;
  }

  resp.response.card = {
    type: "Simple",
    title: options.speechText,
    content: "",
  };
  return resp;
}

function buildAudioDirective(station) {
  const directive = {
    type: 'AudioPlayer.Play',
    playBehavior: 'REPLACE_ALL',
    audioItem: {
      stream: {
        token: station.stationuuid,
        url: station.url,
        offsetInMilliseconds: 0
      }
    },
  };

  // make the click count
  const options = {
    hostname: 'www.radio-browser.info',
    path: `/webservice/v2/json/url/${station.id}`,
    method: 'GET',
    headers: {
      'user-agent': USER_AGENT,
    }
  }
  
  // not waiting here on purpose.
  asyncHttpGet(options)
    .then(() => console.log(`Click count for ${station.id}`))
    .catch(error => console.log(`Error in make click count: ${error.message}`));

  return directive;
}
